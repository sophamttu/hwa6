import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */

public class GraphTask {
   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run(){
      //test1
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (4, 3);
      System.out.println (g);
      Graph.Pair<Vertex,Vertex> p = g.longestPath();
      System.out.println();
      System.out.println("Longest path is from v" + p.first.info + " to v" + p.second.info);

   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex {
      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed
      private int Vinfo; //number of vertices
      private Vertex prev;
      List<Arc> outArces = new ArrayList<Arc>();

      Vertex (String s, Vertex p, Vertex v, Arc e) {
         id = s;
         prev = p;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 1; //weight
      // You can add more fields if needed
      private Vertex source;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      List<Vertex> vertices = new ArrayList<>();
      List<Arc> arces = new ArrayList<>();
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         vertices.add(res);
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.source = from;
         res.target = to;
         to.prev = from;
         from.outArces.add(res);
         arces.add(res);
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
      /*
      public void createRandomDirectedTree (int n) {
      if (n <= 0)
         return;
      Vertex[] varray = new Vertex [n];
      for (int i = 0; i < n; i++) {
         varray [i] = createVertex ("v" + String.valueOf(n-i));
         if (i > 0) {
            int vnr = (int)(Math.random()*i);
            createArc ("a" + varray [vnr].toString() + "_"
                    + varray [i].toString(), varray [vnr], varray [i]);
         } else {}
      }
   }

      public List<Vertex> topoSort() {
         boolean cycleFound = false;
         List order = Collections.synchronizedList(new LinkedList());
         Iterator vit = vertices.iterator();
         while (vit.hasNext()) {
            info++; //number of vertices
            ((Vertex) vit.next()).Vinfo = 0; //number of incoming edges
         }
         Iterator ait = arces.iterator();
         while (ait.hasNext()) {
            Arc a = (Arc) ait.next();
            Vertex target = a.target;
            target.Vinfo++;
         }
         List start = Collections.synchronizedList(new LinkedList());
         vit = vertices.iterator();
         while (vit.hasNext()) {
            Vertex v = (Vertex) vit.next();
            if (v.Vinfo == 0)
               start.add(v);
         }

         if (start.size() == 0) cycleFound = true;
         while ((!cycleFound) & (start.size() != 0)) {
            Vertex current = (Vertex) start.remove(0); // first vertex
            order.add(current);
            ait = current.outArces.iterator(); // "remove" outgoing edges
            while (ait.hasNext()) {
               Arc a = (Arc) ait.next();
               Vertex target = a.target;
               target.Vinfo--;
               if (target.Vinfo == 0) {
                  start.add(target); // no incoming edges anymore
               }
            }
         }
         if (info != order.size()) cycleFound = true;
         if (cycleFound) {
            Iterator it = vertices.iterator();
            while (it.hasNext()) {
               Vertex v = (Vertex) it.next();
               v.info = 0;
            }
         } else {
            int i = 0;
            Iterator it = order.iterator();
            while (it.hasNext()) {
               i++;
               Vertex v = (Vertex) it.next();
               v.info = i;
            }
         }
         return order;
      }

      //find the longest path from a given vertex
      public int findLongestPath(int s) {
         int max = 0;
         List<Vertex> order = topoSort();
         int[][] connected = createAdjMatrix();

         List<Integer> dist = new ArrayList<Integer>();
         while(dist.size() < info)
            dist.add(Integer.MIN_VALUE);
         dist.set(s, 0);
         Iterator it = order.iterator();
         while (it.hasNext()) {
            int length = 0;
            Vertex v = (Vertex) it.next();
            System.out.println(v + " " + v.info);
            Vertex tmp = v;
            if (tmp != null) {
               length++;
               tmp = tmp.prev;
            }
            max = Math.max(length, max);
         }
         return max;
      }
      */

      /**
       *
       * @param info the numerical id of the vertex
       * @return vertex with that id
       */
      public Vertex find(int info) {
         Iterator<Vertex> vit = vertices.iterator();
         while(vit.hasNext()) {
            Vertex v = (Vertex) vit.next();
            if(v.info == info)
               return v;
         }
         return new Vertex(null);
      }

      /**
       * A pair of vertices
       * @param <U> Vertex
       * @param <V> Vertex
       */
      public class Pair<U,V> {
         public U first;
         public V second;
         public Pair(U first, V second) {
            this.first = first;
            this.second = second;
         }
      }

      /**
       *
       * @return pair of vertices with longest path
       */
      public Pair<Vertex,Vertex> longestPath() {
         int v_num = vertices.size();
         int[][] dist = new int[v_num][v_num];
         Iterator<Arc> ait = arces.iterator();
         while(ait.hasNext()) {
            Arc a = (Arc)ait.next();
            Vertex i = a.source;
            Vertex j = a.target;
            dist[i.info][j.info] = a.info;
         }
         Iterator<Vertex> vit = vertices.iterator();
         while(vit.hasNext()) {
            Vertex v = (Vertex)vit.next();
            dist[v.info][v.info] = 0;
         }
         int max = 0;
         int v1 = 0, v2 = 0;
         for(int k = 1; k < v_num; k++) {
            for(int i = 1; i < v_num; i++) {
               for(int j = 1; j < v_num; j++) {
                  if(dist[i][j] < dist[i][k] + dist[k][i] && i != j)
                     dist[i][j] = dist[i][k] + dist[k][j];
                     if(dist[i][j] > max) {
                        v1 = i;
                        v2 = j;
                        max = dist[i][j];
                     }
               }
            }
         }
         System.out.println("Distance matrix:");
         for(int i = 0; i < dist.length; i++) {
            for(int j = 0; j < dist[i].length; j++) {
               System.out.print(dist[i][j] + " ");
            }
            System.out.println();
         }
         Vertex s = find(v1);
         Vertex t = find(v2);
         return new Pair(s,t);
      }
   }
} 

